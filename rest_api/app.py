'''
Consumer app
'''
import os
import json
from flask import Flask, request, jsonify
from kafka import KafkaConsumer


app = Flask(__name__)

@app.route('/fetch/<string:topic>/<int:gid>', methods=['GET'])
def fetchMessage(topic, gid):
    consumer = KafkaConsumer(
        topic, 
        group_id=str(gid),
        bootstrap_servers=[os.getenv('KAFKA_BROKER')],
        value_deserializer=lambda m: json.loads(m.decode('utf-8')),
        auto_commit_interval_ms=100,
        consumer_timeout_ms=500
    )
    message = {}
    for m in consumer:
        print ("%s:%d:%d: key=%s value=%s" % (m.topic, m.partition,
                                          m.offset, m.key,
                                          m.value))
        message = m
        break
    consumer.close()
    return jsonify(message), 200

if __name__ == "__main__":
    app.debug = True
    app.run('0.0.0.0')