'''
Processor app
'''
from flask import Flask, request, jsonify
import profanity_check as pc


app = Flask(__name__)

@app.route('/check_profanity', methods=['POST'])
def checkProfanity():
    text = request.get_json().get('text')
    if text:
        return str(pc.predict([text])[0])
    return 'bad request', 400

if __name__ == "__main__":
    app.debug = True
    app.run('0.0.0.0')