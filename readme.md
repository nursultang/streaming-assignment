# INTRUCTIONS

**Assuming there are working Kafka cluster in local environment <https://docs.confluent.io/current/quickstart/ce-docker-quickstart.html> and working NiFi minimal local setup <https://www.nifi.rocks/official-nifi-docker-image/>**

>**Main workflow**

1. Make sure all of the conainers are in the <u>same virtual network</u> (***e.g. pipeline***)
2. Run `docker-compose up -d --build` in project root folder
3. Import NiFi template file **kafka_flow.xml** into NiFi workspace
4. Start all processors

>**Simple producer client**
0. Add <u>broker</u> address to your <u>*hosts*</u> file (***e.g. 192.168.99.100  broker***) // related to MacOS
1. Goto ***datagen*** directory
2. Run `. env/bin/activate`
3. Run `export KAFKA_BROKER={your kafka broker host and port}` // ***same value must be provided in <u>docker-compose.yml</u> file***
4. Run `python app.py`