import sys
import os
from kafka import KafkaProducer
from kafka.errors import KafkaError

def on_send_success(record_metadata):
    print(record_metadata.topic, record_metadata.offset)

def on_send_error(excp):
    print('error publishing', exc_info=excp)

if __name__ == "__main__":
    topic = sys.argv[1]
    producer = KafkaProducer(bootstrap_servers=[os.getenv('KAFKA_BROKER')])
    print('Enter message and hit ENTER')
    while True:
        try:
            message = input()
            res = producer.send(topic, message.encode()).add_callback(on_send_success).add_errback(on_send_error)
            try:
                record_metadata = res.get(timeout=10)
            except KafkaError as e:
                print(e)
        except KeyboardInterrupt:
            sys.exit(0)
